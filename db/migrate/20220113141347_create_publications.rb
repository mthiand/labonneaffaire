class CreatePublications < ActiveRecord::Migration[7.0]
  def change
    create_table :publications do |t|
      t.string :titre
      t.text :description
      t.string :typeannonce
      t.float :prix
      t.boolean :livrable
      t.string :adresse
      t.float :latitude
      t.float :longitude
      t.references :paysannonce, null: false, foreign_key: true
      t.references :region, null: false, foreign_key: true
      t.references :departement, null: false, foreign_key: true
      t.references :arrondissement, null: false, foreign_key: true
      t.references :classification, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :icone_file_name
      t.integer :icone_file_size
      t.string :icone_content_type
      t.datetime :icone_updated_at

      t.timestamps
    end
  end
end
