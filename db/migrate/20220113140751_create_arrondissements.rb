class CreateArrondissements < ActiveRecord::Migration[7.0]
  def change
    create_table :arrondissements do |t|
      t.string :code
      t.string :nom
      t.float :latitude
      t.float :longitude
      t.references :departement, null: false, foreign_key: true

      t.timestamps
    end
  end
end
