class CreateDepartements < ActiveRecord::Migration[7.0]
  def change
    create_table :departements do |t|
      t.string :code
      t.string :nom
      t.float :latitude
      t.float :longitude
      t.references :region, null: false, foreign_key: true

      t.timestamps
    end
  end
end
