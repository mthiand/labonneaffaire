class CreatePiecejointes < ActiveRecord::Migration[7.0]
  def change
    create_table :piecejointes do |t|
      t.string :nom
      t.string :pj_file_name
      t.integer :pj_file_size
      t.string :pj_content_type
      t.datetime :pj_updated_at
      t.references :publication, null: false, foreign_key: true

      t.timestamps
    end
  end
end
