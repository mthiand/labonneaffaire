class CreateAttributclassifications < ActiveRecord::Migration[7.0]
  def change
    create_table :attributclassifications do |t|
      t.string :code
      t.string :libelle
      t.string :type
      t.integer :longueur
      t.integer :nbdecimal
      t.boolean :zonefiltre
      t.references :classification, null: false, foreign_key: true

      t.timestamps
    end
  end
end
