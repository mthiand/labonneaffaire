class CreateClassifications < ActiveRecord::Migration[7.0]
  def change
    create_table :classifications do |t|
      t.string :code
      t.string :libelle
      t.string :icone_file_name
      t.integer :icone_file_size
      t.string :icone_content_type
      t.datetime :icone_updated_at
      t.references :classification, null: true, foreign_key: true

      t.timestamps
    end
  end
end
