class AddAttachmentIconeToPublications < ActiveRecord::Migration[7.0]
  def self.up
    change_table :publications do |t|
      add_column :publications, :icone_file_name, :string
      add_column :publications, :icone_file_size, :integer
      add_column :publications, :icone_content_type, :string
      add_column :publications, :icone_updated_at, :datetime
      end
  end

  def self.down
    remove_attachment :publications, :icone
  end
end
