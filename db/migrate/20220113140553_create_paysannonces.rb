class CreatePaysannonces < ActiveRecord::Migration[7.0]
  def change
    create_table :paysannonces do |t|
      t.string :code
      t.string :nom
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
