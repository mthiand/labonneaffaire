class CreateAttributpublications < ActiveRecord::Migration[7.0]
  def change
    create_table :attributpublications do |t|
      t.string :valeur
      t.references :attributclassification, null: false, foreign_key: true
      t.references :publication, null: false, foreign_key: true

      t.timestamps
    end
  end
end
