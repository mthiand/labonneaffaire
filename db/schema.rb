# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_13_174050) do

  create_table "arrondissements", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.float "latitude"
    t.float "longitude"
    t.integer "departement_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["departement_id"], name: "index_arrondissements_on_departement_id"
  end

  create_table "attributclassifications", force: :cascade do |t|
    t.string "code"
    t.string "libelle"
    t.string "type"
    t.integer "longueur"
    t.integer "nbdecimal"
    t.boolean "zonefiltre"
    t.integer "classification_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["classification_id"], name: "index_attributclassifications_on_classification_id"
  end

  create_table "attributpublications", force: :cascade do |t|
    t.string "valeur"
    t.integer "attributclassification_id", null: false
    t.integer "publication_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attributclassification_id"], name: "index_attributpublications_on_attributclassification_id"
    t.index ["publication_id"], name: "index_attributpublications_on_publication_id"
  end

  create_table "classifications", force: :cascade do |t|
    t.string "code"
    t.string "libelle"
    t.string "icone_file_name"
    t.integer "icone_file_size"
    t.string "icone_content_type"
    t.datetime "icone_updated_at", precision: 6
    t.integer "classification_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["classification_id"], name: "index_classifications_on_classification_id"
  end

  create_table "communes", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.float "latitude"
    t.float "longitude"
    t.integer "arrondissement_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["arrondissement_id"], name: "index_communes_on_arrondissement_id"
  end

  create_table "departements", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.float "latitude"
    t.float "longitude"
    t.integer "region_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["region_id"], name: "index_departements_on_region_id"
  end

  create_table "paysannonces", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "piecejointes", force: :cascade do |t|
    t.string "nom"
    t.string "pj_file_name"
    t.integer "pj_file_size"
    t.string "pj_content_type"
    t.datetime "pj_updated_at", precision: 6
    t.integer "publication_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["publication_id"], name: "index_piecejointes_on_publication_id"
  end

  create_table "publications", force: :cascade do |t|
    t.string "titre"
    t.text "description"
    t.string "typeannonce"
    t.float "prix"
    t.boolean "livrable"
    t.string "adresse"
    t.float "latitude"
    t.float "longitude"
    t.integer "paysannonce_id", null: false
    t.integer "region_id", null: false
    t.integer "departement_id"
    t.integer "arrondissement_id"
    t.integer "classification_id", null: false
    t.integer "user_id", null: false
    t.string "icone_file_name"
    t.integer "icone_file_size"
    t.string "icone_content_type"
    t.datetime "icone_updated_at", precision: 6
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["arrondissement_id"], name: "index_publications_on_arrondissement_id"
    t.index ["classification_id"], name: "index_publications_on_classification_id"
    t.index ["departement_id"], name: "index_publications_on_departement_id"
    t.index ["paysannonce_id"], name: "index_publications_on_paysannonce_id"
    t.index ["region_id"], name: "index_publications_on_region_id"
    t.index ["user_id"], name: "index_publications_on_user_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.float "latitude"
    t.float "longitude"
    t.integer "paysannonce_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["paysannonce_id"], name: "index_regions_on_paysannonce_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "contact"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "arrondissements", "departements"
  add_foreign_key "attributclassifications", "classifications"
  add_foreign_key "attributpublications", "attributclassifications"
  add_foreign_key "attributpublications", "publications"
  add_foreign_key "classifications", "classifications"
  add_foreign_key "communes", "arrondissements"
  add_foreign_key "departements", "regions"
  add_foreign_key "piecejointes", "publications"
  add_foreign_key "publications", "arrondissements"
  add_foreign_key "publications", "classifications"
  add_foreign_key "publications", "departements"
  add_foreign_key "publications", "paysannonces"
  add_foreign_key "publications", "regions"
  add_foreign_key "publications", "users"
  add_foreign_key "regions", "paysannonces"
end
