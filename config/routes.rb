Rails.application.routes.draw do
  resources :piecejointes
  resources :attributpublications
  resources :publications, only: [:new, :create, :edit, :show, :update, :destroy] do
    get :search, on: :collection
  end
  resources :users
  resources :attributclassifications
  resources :classifications
  resources :communes
  resources :arrondissements
  resources :departements
  resources :regions
  resources :paysannonces
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get 'sessions/new'
  get '/leboncoin', to: 'site#home'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/logout', to: 'sessions#destroy'

  # Defines the root path route ("/")
  root "publications#search"
end
