require "application_system_test_case"

class PublicationsTest < ApplicationSystemTestCase
  setup do
    @publication = publications(:one)
  end

  test "visiting the index" do
    visit publications_url
    assert_selector "h1", text: "Publications"
  end

  test "should create publication" do
    visit publications_url
    click_on "New publication"

    fill_in "Adresse", with: @publication.adresse
    fill_in "Arrondissement", with: @publication.arrondissement_id
    fill_in "Classification", with: @publication.classification_id
    fill_in "Departement", with: @publication.departement_id
    fill_in "Description", with: @publication.description
    fill_in "Icone content type", with: @publication.icone_content_type
    fill_in "Icone file name", with: @publication.icone_file_name
    fill_in "Icone file size", with: @publication.icone_file_size
    fill_in "Icone updated at", with: @publication.icone_updated_at
    fill_in "Latitude", with: @publication.latitude
    check "Livrable" if @publication.livrable
    fill_in "Longitude", with: @publication.longitude
    fill_in "Paysannonce", with: @publication.paysannonce_id
    fill_in "Prix", with: @publication.prix
    fill_in "Region", with: @publication.region_id
    fill_in "Titre", with: @publication.titre
    fill_in "Type", with: @publication.type
    fill_in "User", with: @publication.user_id
    click_on "Create Publication"

    assert_text "Publication was successfully created"
    click_on "Back"
  end

  test "should update Publication" do
    visit publication_url(@publication)
    click_on "Edit this publication", match: :first

    fill_in "Adresse", with: @publication.adresse
    fill_in "Arrondissement", with: @publication.arrondissement_id
    fill_in "Classification", with: @publication.classification_id
    fill_in "Departement", with: @publication.departement_id
    fill_in "Description", with: @publication.description
    fill_in "Icone content type", with: @publication.icone_content_type
    fill_in "Icone file name", with: @publication.icone_file_name
    fill_in "Icone file size", with: @publication.icone_file_size
    fill_in "Icone updated at", with: @publication.icone_updated_at
    fill_in "Latitude", with: @publication.latitude
    check "Livrable" if @publication.livrable
    fill_in "Longitude", with: @publication.longitude
    fill_in "Paysannonce", with: @publication.paysannonce_id
    fill_in "Prix", with: @publication.prix
    fill_in "Region", with: @publication.region_id
    fill_in "Titre", with: @publication.titre
    fill_in "Type", with: @publication.type
    fill_in "User", with: @publication.user_id
    click_on "Update Publication"

    assert_text "Publication was successfully updated"
    click_on "Back"
  end

  test "should destroy Publication" do
    visit publication_url(@publication)
    click_on "Destroy this publication", match: :first

    assert_text "Publication was successfully destroyed"
  end
end
