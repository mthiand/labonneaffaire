require "application_system_test_case"

class AttributpublicationsTest < ApplicationSystemTestCase
  setup do
    @attributpublication = attributpublications(:one)
  end

  test "visiting the index" do
    visit attributpublications_url
    assert_selector "h1", text: "Attributpublications"
  end

  test "should create attributpublication" do
    visit attributpublications_url
    click_on "New attributpublication"

    fill_in "Attributclassification", with: @attributpublication.attributclassification_id
    fill_in "Publication", with: @attributpublication.publication_id
    fill_in "Valeur", with: @attributpublication.valeur
    click_on "Create Attributpublication"

    assert_text "Attributpublication was successfully created"
    click_on "Back"
  end

  test "should update Attributpublication" do
    visit attributpublication_url(@attributpublication)
    click_on "Edit this attributpublication", match: :first

    fill_in "Attributclassification", with: @attributpublication.attributclassification_id
    fill_in "Publication", with: @attributpublication.publication_id
    fill_in "Valeur", with: @attributpublication.valeur
    click_on "Update Attributpublication"

    assert_text "Attributpublication was successfully updated"
    click_on "Back"
  end

  test "should destroy Attributpublication" do
    visit attributpublication_url(@attributpublication)
    click_on "Destroy this attributpublication", match: :first

    assert_text "Attributpublication was successfully destroyed"
  end
end
