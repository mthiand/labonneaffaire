require "application_system_test_case"

class AttributclassificationsTest < ApplicationSystemTestCase
  setup do
    @attributclassification = attributclassifications(:one)
  end

  test "visiting the index" do
    visit attributclassifications_url
    assert_selector "h1", text: "Attributclassifications"
  end

  test "should create attributclassification" do
    visit attributclassifications_url
    click_on "New attributclassification"

    fill_in "Classification", with: @attributclassification.classification_id
    fill_in "Code", with: @attributclassification.code
    fill_in "Libelle", with: @attributclassification.libelle
    fill_in "Longueur", with: @attributclassification.longueur
    fill_in "Nbdecimal", with: @attributclassification.nbdecimal
    fill_in "Type", with: @attributclassification.type
    check "Zonefiltre" if @attributclassification.zonefiltre
    click_on "Create Attributclassification"

    assert_text "Attributclassification was successfully created"
    click_on "Back"
  end

  test "should update Attributclassification" do
    visit attributclassification_url(@attributclassification)
    click_on "Edit this attributclassification", match: :first

    fill_in "Classification", with: @attributclassification.classification_id
    fill_in "Code", with: @attributclassification.code
    fill_in "Libelle", with: @attributclassification.libelle
    fill_in "Longueur", with: @attributclassification.longueur
    fill_in "Nbdecimal", with: @attributclassification.nbdecimal
    fill_in "Type", with: @attributclassification.type
    check "Zonefiltre" if @attributclassification.zonefiltre
    click_on "Update Attributclassification"

    assert_text "Attributclassification was successfully updated"
    click_on "Back"
  end

  test "should destroy Attributclassification" do
    visit attributclassification_url(@attributclassification)
    click_on "Destroy this attributclassification", match: :first

    assert_text "Attributclassification was successfully destroyed"
  end
end
