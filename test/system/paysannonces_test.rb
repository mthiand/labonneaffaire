require "application_system_test_case"

class PaysannoncesTest < ApplicationSystemTestCase
  setup do
    @paysannonce = paysannonces(:one)
  end

  test "visiting the index" do
    visit paysannonces_url
    assert_selector "h1", text: "Paysannonces"
  end

  test "should create paysannonce" do
    visit paysannonces_url
    click_on "New paysannonce"

    fill_in "Code", with: @paysannonce.code
    fill_in "Latitude", with: @paysannonce.latitude
    fill_in "Longitude", with: @paysannonce.longitude
    fill_in "Nom", with: @paysannonce.nom
    click_on "Create Paysannonce"

    assert_text "Paysannonce was successfully created"
    click_on "Back"
  end

  test "should update Paysannonce" do
    visit paysannonce_url(@paysannonce)
    click_on "Edit this paysannonce", match: :first

    fill_in "Code", with: @paysannonce.code
    fill_in "Latitude", with: @paysannonce.latitude
    fill_in "Longitude", with: @paysannonce.longitude
    fill_in "Nom", with: @paysannonce.nom
    click_on "Update Paysannonce"

    assert_text "Paysannonce was successfully updated"
    click_on "Back"
  end

  test "should destroy Paysannonce" do
    visit paysannonce_url(@paysannonce)
    click_on "Destroy this paysannonce", match: :first

    assert_text "Paysannonce was successfully destroyed"
  end
end
