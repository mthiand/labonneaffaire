require "application_system_test_case"

class PiecejointesTest < ApplicationSystemTestCase
  setup do
    @piecejointe = piecejointes(:one)
  end

  test "visiting the index" do
    visit piecejointes_url
    assert_selector "h1", text: "Piecejointes"
  end

  test "should create piecejointe" do
    visit piecejointes_url
    click_on "New piecejointe"

    fill_in "Nom", with: @piecejointe.nom
    fill_in "Pj content type", with: @piecejointe.pj_content_type
    fill_in "Pj file name", with: @piecejointe.pj_file_name
    fill_in "Pj file size", with: @piecejointe.pj_file_size
    fill_in "Pj updated at", with: @piecejointe.pj_updated_at
    fill_in "Publication", with: @piecejointe.publication_id
    click_on "Create Piecejointe"

    assert_text "Piecejointe was successfully created"
    click_on "Back"
  end

  test "should update Piecejointe" do
    visit piecejointe_url(@piecejointe)
    click_on "Edit this piecejointe", match: :first

    fill_in "Nom", with: @piecejointe.nom
    fill_in "Pj content type", with: @piecejointe.pj_content_type
    fill_in "Pj file name", with: @piecejointe.pj_file_name
    fill_in "Pj file size", with: @piecejointe.pj_file_size
    fill_in "Pj updated at", with: @piecejointe.pj_updated_at
    fill_in "Publication", with: @piecejointe.publication_id
    click_on "Update Piecejointe"

    assert_text "Piecejointe was successfully updated"
    click_on "Back"
  end

  test "should destroy Piecejointe" do
    visit piecejointe_url(@piecejointe)
    click_on "Destroy this piecejointe", match: :first

    assert_text "Piecejointe was successfully destroyed"
  end
end
