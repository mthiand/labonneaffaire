require "application_system_test_case"

class ArrondissementsTest < ApplicationSystemTestCase
  setup do
    @arrondissement = arrondissements(:one)
  end

  test "visiting the index" do
    visit arrondissements_url
    assert_selector "h1", text: "Arrondissements"
  end

  test "should create arrondissement" do
    visit arrondissements_url
    click_on "New arrondissement"

    fill_in "Code", with: @arrondissement.code
    fill_in "Departement", with: @arrondissement.departement_id
    fill_in "Latitude", with: @arrondissement.latitude
    fill_in "Longitude", with: @arrondissement.longitude
    fill_in "Nom", with: @arrondissement.nom
    click_on "Create Arrondissement"

    assert_text "Arrondissement was successfully created"
    click_on "Back"
  end

  test "should update Arrondissement" do
    visit arrondissement_url(@arrondissement)
    click_on "Edit this arrondissement", match: :first

    fill_in "Code", with: @arrondissement.code
    fill_in "Departement", with: @arrondissement.departement_id
    fill_in "Latitude", with: @arrondissement.latitude
    fill_in "Longitude", with: @arrondissement.longitude
    fill_in "Nom", with: @arrondissement.nom
    click_on "Update Arrondissement"

    assert_text "Arrondissement was successfully updated"
    click_on "Back"
  end

  test "should destroy Arrondissement" do
    visit arrondissement_url(@arrondissement)
    click_on "Destroy this arrondissement", match: :first

    assert_text "Arrondissement was successfully destroyed"
  end
end
