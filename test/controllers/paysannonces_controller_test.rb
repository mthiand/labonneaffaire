require "test_helper"

class PaysannoncesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @paysannonce = paysannonces(:one)
  end

  test "should get index" do
    get paysannonces_url
    assert_response :success
  end

  test "should get new" do
    get new_paysannonce_url
    assert_response :success
  end

  test "should create paysannonce" do
    assert_difference("Paysannonce.count") do
      post paysannonces_url, params: { paysannonce: { code: @paysannonce.code, latitude: @paysannonce.latitude, longitude: @paysannonce.longitude, nom: @paysannonce.nom } }
    end

    assert_redirected_to paysannonce_url(Paysannonce.last)
  end

  test "should show paysannonce" do
    get paysannonce_url(@paysannonce)
    assert_response :success
  end

  test "should get edit" do
    get edit_paysannonce_url(@paysannonce)
    assert_response :success
  end

  test "should update paysannonce" do
    patch paysannonce_url(@paysannonce), params: { paysannonce: { code: @paysannonce.code, latitude: @paysannonce.latitude, longitude: @paysannonce.longitude, nom: @paysannonce.nom } }
    assert_redirected_to paysannonce_url(@paysannonce)
  end

  test "should destroy paysannonce" do
    assert_difference("Paysannonce.count", -1) do
      delete paysannonce_url(@paysannonce)
    end

    assert_redirected_to paysannonces_url
  end
end
