require "test_helper"

class AttributpublicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attributpublication = attributpublications(:one)
  end

  test "should get index" do
    get attributpublications_url
    assert_response :success
  end

  test "should get new" do
    get new_attributpublication_url
    assert_response :success
  end

  test "should create attributpublication" do
    assert_difference("Attributpublication.count") do
      post attributpublications_url, params: { attributpublication: { attributclassification_id: @attributpublication.attributclassification_id, publication_id: @attributpublication.publication_id, valeur: @attributpublication.valeur } }
    end

    assert_redirected_to attributpublication_url(Attributpublication.last)
  end

  test "should show attributpublication" do
    get attributpublication_url(@attributpublication)
    assert_response :success
  end

  test "should get edit" do
    get edit_attributpublication_url(@attributpublication)
    assert_response :success
  end

  test "should update attributpublication" do
    patch attributpublication_url(@attributpublication), params: { attributpublication: { attributclassification_id: @attributpublication.attributclassification_id, publication_id: @attributpublication.publication_id, valeur: @attributpublication.valeur } }
    assert_redirected_to attributpublication_url(@attributpublication)
  end

  test "should destroy attributpublication" do
    assert_difference("Attributpublication.count", -1) do
      delete attributpublication_url(@attributpublication)
    end

    assert_redirected_to attributpublications_url
  end
end
