require "test_helper"

class PublicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @publication = publications(:one)
  end

  test "should get index" do
    get publications_url
    assert_response :success
  end

  test "should get new" do
    get new_publication_url
    assert_response :success
  end

  test "should create publication" do
    assert_difference("Publication.count") do
      post publications_url, params: { publication: { adresse: @publication.adresse, arrondissement_id: @publication.arrondissement_id, classification_id: @publication.classification_id, departement_id: @publication.departement_id, description: @publication.description, icone_content_type: @publication.icone_content_type, icone_file_name: @publication.icone_file_name, icone_file_size: @publication.icone_file_size, icone_updated_at: @publication.icone_updated_at, latitude: @publication.latitude, livrable: @publication.livrable, longitude: @publication.longitude, paysannonce_id: @publication.paysannonce_id, prix: @publication.prix, region_id: @publication.region_id, titre: @publication.titre, type: @publication.type, user_id: @publication.user_id } }
    end

    assert_redirected_to publication_url(Publication.last)
  end

  test "should show publication" do
    get publication_url(@publication)
    assert_response :success
  end

  test "should get edit" do
    get edit_publication_url(@publication)
    assert_response :success
  end

  test "should update publication" do
    patch publication_url(@publication), params: { publication: { adresse: @publication.adresse, arrondissement_id: @publication.arrondissement_id, classification_id: @publication.classification_id, departement_id: @publication.departement_id, description: @publication.description, icone_content_type: @publication.icone_content_type, icone_file_name: @publication.icone_file_name, icone_file_size: @publication.icone_file_size, icone_updated_at: @publication.icone_updated_at, latitude: @publication.latitude, livrable: @publication.livrable, longitude: @publication.longitude, paysannonce_id: @publication.paysannonce_id, prix: @publication.prix, region_id: @publication.region_id, titre: @publication.titre, type: @publication.type, user_id: @publication.user_id } }
    assert_redirected_to publication_url(@publication)
  end

  test "should destroy publication" do
    assert_difference("Publication.count", -1) do
      delete publication_url(@publication)
    end

    assert_redirected_to publications_url
  end
end
