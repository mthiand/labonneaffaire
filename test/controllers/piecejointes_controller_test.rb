require "test_helper"

class PiecejointesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @piecejointe = piecejointes(:one)
  end

  test "should get index" do
    get piecejointes_url
    assert_response :success
  end

  test "should get new" do
    get new_piecejointe_url
    assert_response :success
  end

  test "should create piecejointe" do
    assert_difference("Piecejointe.count") do
      post piecejointes_url, params: { piecejointe: { nom: @piecejointe.nom, pj_content_type: @piecejointe.pj_content_type, pj_file_name: @piecejointe.pj_file_name, pj_file_size: @piecejointe.pj_file_size, pj_updated_at: @piecejointe.pj_updated_at, publication_id: @piecejointe.publication_id } }
    end

    assert_redirected_to piecejointe_url(Piecejointe.last)
  end

  test "should show piecejointe" do
    get piecejointe_url(@piecejointe)
    assert_response :success
  end

  test "should get edit" do
    get edit_piecejointe_url(@piecejointe)
    assert_response :success
  end

  test "should update piecejointe" do
    patch piecejointe_url(@piecejointe), params: { piecejointe: { nom: @piecejointe.nom, pj_content_type: @piecejointe.pj_content_type, pj_file_name: @piecejointe.pj_file_name, pj_file_size: @piecejointe.pj_file_size, pj_updated_at: @piecejointe.pj_updated_at, publication_id: @piecejointe.publication_id } }
    assert_redirected_to piecejointe_url(@piecejointe)
  end

  test "should destroy piecejointe" do
    assert_difference("Piecejointe.count", -1) do
      delete piecejointe_url(@piecejointe)
    end

    assert_redirected_to piecejointes_url
  end
end
