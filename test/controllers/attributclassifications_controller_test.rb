require "test_helper"

class AttributclassificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attributclassification = attributclassifications(:one)
  end

  test "should get index" do
    get attributclassifications_url
    assert_response :success
  end

  test "should get new" do
    get new_attributclassification_url
    assert_response :success
  end

  test "should create attributclassification" do
    assert_difference("Attributclassification.count") do
      post attributclassifications_url, params: { attributclassification: { classification_id: @attributclassification.classification_id, code: @attributclassification.code, libelle: @attributclassification.libelle, longueur: @attributclassification.longueur, nbdecimal: @attributclassification.nbdecimal, type: @attributclassification.type, zonefiltre: @attributclassification.zonefiltre } }
    end

    assert_redirected_to attributclassification_url(Attributclassification.last)
  end

  test "should show attributclassification" do
    get attributclassification_url(@attributclassification)
    assert_response :success
  end

  test "should get edit" do
    get edit_attributclassification_url(@attributclassification)
    assert_response :success
  end

  test "should update attributclassification" do
    patch attributclassification_url(@attributclassification), params: { attributclassification: { classification_id: @attributclassification.classification_id, code: @attributclassification.code, libelle: @attributclassification.libelle, longueur: @attributclassification.longueur, nbdecimal: @attributclassification.nbdecimal, type: @attributclassification.type, zonefiltre: @attributclassification.zonefiltre } }
    assert_redirected_to attributclassification_url(@attributclassification)
  end

  test "should destroy attributclassification" do
    assert_difference("Attributclassification.count", -1) do
      delete attributclassification_url(@attributclassification)
    end

    assert_redirected_to attributclassifications_url
  end
end
