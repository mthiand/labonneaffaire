class Classification < ApplicationRecord
  validates :code, presence: true
  validates :libelle, presence: true

  has_attached_file :icone, styles: { medium: "250x250>", thumb: "50x50>" }, default_url: "/images/:style/missing.png"
  #do_not_validate_attachment_file_type :pj
  validates_attachment_content_type :icone, content_type: /\Aimage\/.*\z/

  has_many :classifications

  belongs_to :classification, optional: true
end
