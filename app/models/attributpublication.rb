class Attributpublication < ApplicationRecord
  validates :code, presence: true
  validates :libelle, presence: true

  belongs_to :attributclassification
  belongs_to :publication
end
