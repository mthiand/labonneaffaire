class Paysannonce < ApplicationRecord
    validates :code, presence: true
    validates :nom, presence: true

    has_many :regions, dependent: :destroy

end
