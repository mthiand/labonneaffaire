class Arrondissement < ApplicationRecord
  validates :code, presence: true
  validates :nom, presence: true

  has_many :communes, dependent: :destroy

  belongs_to :departement
end
