class Departement < ApplicationRecord
  validates :code, presence: true
  validates :nom, presence: true

  has_many :arrondissements, dependent: :destroy

  belongs_to :region
end
