class Piecejointe < ApplicationRecord
  has_attached_file :pj, styles: { medium: "250x250>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  #do_not_validate_attachment_file_type :pj
  validates_attachment_content_type :pj, content_type: /\Aimage\/.*\z/

  belongs_to :publication
end
