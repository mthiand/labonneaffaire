class Region < ApplicationRecord
  validates :code, presence: true
  validates :nom, presence: true

  has_many :departements, dependent: :destroy

  belongs_to :paysannonce
end
