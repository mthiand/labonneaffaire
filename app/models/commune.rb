class Commune < ApplicationRecord
  validates :code, presence: true
  validates :nom, presence: true

  belongs_to :arrondissement
end
