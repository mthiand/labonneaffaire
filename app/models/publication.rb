class Publication < ApplicationRecord
  validates :titre, presence: true
  validates :description, presence: true
  validates :adresse, presence: true

  has_attached_file :icone, styles: { medium: "250x250>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  #do_not_validate_attachment_file_type :icone
  validates_attachment_content_type :icone, content_type: /\Aimage\/.*\z/, validate_media_type: false

  has_many :piecejointes, dependent: :destroy

  belongs_to :paysannonce
  belongs_to :region
  belongs_to :departement
  belongs_to :arrondissement
  belongs_to :classification
  belongs_to :user
end
