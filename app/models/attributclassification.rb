class Attributclassification < ApplicationRecord
  validates :code, presence: true
  validates :libelle, presence: true

  belongs_to :classification
end
