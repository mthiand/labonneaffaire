// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

import "rails-ujs"
import "turbolinks"
//= require_tree
import "bootstrap-sprockets"
import "bootstrap"
import "bootstrap.bundle.min"