json.extract! publication, :id, :titre, :description, :type, :prix, :livrable, :adresse, :latitude, :longitude, :paysannonce_id, :region_id, :departement_id, :arrondissement_id, :classification_id, :user_id, :icone_file_name, :icone_file_size, :icone_content_type, :icone_updated_at, :created_at, :updated_at
json.url publication_url(publication, format: :json)
