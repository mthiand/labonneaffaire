json.extract! commune, :id, :code, :nom, :latitude, :longitude, :arrondissement_id, :created_at, :updated_at
json.url commune_url(commune, format: :json)
