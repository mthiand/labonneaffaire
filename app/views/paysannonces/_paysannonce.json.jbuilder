json.extract! paysannonce, :id, :code, :nom, :latitude, :longitude, :created_at, :updated_at
json.url paysannonce_url(paysannonce, format: :json)
