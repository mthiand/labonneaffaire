json.extract! classification, :id, :code, :libelle, :icone_file_name, :icone_file_size, :icone_content_type, :icone_updated_at, :classification_id, :created_at, :updated_at
json.url classification_url(classification, format: :json)
