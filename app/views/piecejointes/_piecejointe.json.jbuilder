json.extract! piecejointe, :id, :nom, :pj_file_name, :pj_file_size, :pj_content_type, :pj_updated_at, :publication_id, :created_at, :updated_at
json.url piecejointe_url(piecejointe, format: :json)
