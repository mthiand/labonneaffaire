json.extract! arrondissement, :id, :code, :nom, :latitude, :longitude, :departement_id, :created_at, :updated_at
json.url arrondissement_url(arrondissement, format: :json)
