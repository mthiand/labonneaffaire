json.extract! departement, :id, :code, :nom, :latitude, :longitude, :region_id, :created_at, :updated_at
json.url departement_url(departement, format: :json)
