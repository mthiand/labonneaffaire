json.extract! attributclassification, :id, :code, :libelle, :type, :longueur, :nbdecimal, :zonefiltre, :classification_id, :created_at, :updated_at
json.url attributclassification_url(attributclassification, format: :json)
