class ArrondissementsController < ApplicationController
  before_action :set_arrondissement, only: %i[ show edit update destroy ]

  # GET /arrondissements or /arrondissements.json
  def index
    @arrondissements = Arrondissement.all
  end

  # GET /arrondissements/1 or /arrondissements/1.json
  def show
  end

  # GET /arrondissements/new
  def new
    @arrondissement = Arrondissement.new
  end

  # GET /arrondissements/1/edit
  def edit
  end

  # POST /arrondissements or /arrondissements.json
  def create
    @arrondissement = Arrondissement.new(arrondissement_params)

    respond_to do |format|
      if @arrondissement.save
        format.html { redirect_to arrondissement_url(@arrondissement), notice: "Arrondissement was successfully created." }
        format.json { render :show, status: :created, location: @arrondissement }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @arrondissement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /arrondissements/1 or /arrondissements/1.json
  def update
    respond_to do |format|
      if @arrondissement.update(arrondissement_params)
        format.html { redirect_to arrondissement_url(@arrondissement), notice: "Arrondissement was successfully updated." }
        format.json { render :show, status: :ok, location: @arrondissement }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @arrondissement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /arrondissements/1 or /arrondissements/1.json
  def destroy
    @arrondissement.destroy

    respond_to do |format|
      format.html { redirect_to arrondissements_url, notice: "Arrondissement was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_arrondissement
      @arrondissement = Arrondissement.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def arrondissement_params
      params.require(:arrondissement).permit(:code, :nom, :latitude, :longitude, :departement_id)
    end
end
