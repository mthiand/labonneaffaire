class AttributpublicationsController < ApplicationController
  before_action :set_attributpublication, only: %i[ show edit update destroy ]

  # GET /attributpublications or /attributpublications.json
  def index
    @attributpublications = Attributpublication.all
  end

  # GET /attributpublications/1 or /attributpublications/1.json
  def show
  end

  # GET /attributpublications/new
  def new
    @attributpublication = Attributpublication.new
  end

  # GET /attributpublications/1/edit
  def edit
  end

  # POST /attributpublications or /attributpublications.json
  def create
    @attributpublication = Attributpublication.new(attributpublication_params)

    respond_to do |format|
      if @attributpublication.save
        format.html { redirect_to attributpublication_url(@attributpublication), notice: "Attributpublication was successfully created." }
        format.json { render :show, status: :created, location: @attributpublication }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @attributpublication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attributpublications/1 or /attributpublications/1.json
  def update
    respond_to do |format|
      if @attributpublication.update(attributpublication_params)
        format.html { redirect_to attributpublication_url(@attributpublication), notice: "Attributpublication was successfully updated." }
        format.json { render :show, status: :ok, location: @attributpublication }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @attributpublication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attributpublications/1 or /attributpublications/1.json
  def destroy
    @attributpublication.destroy

    respond_to do |format|
      format.html { redirect_to attributpublications_url, notice: "Attributpublication was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attributpublication
      @attributpublication = Attributpublication.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def attributpublication_params
      params.require(:attributpublication).permit(:valeur, :attributclassification_id, :publication_id)
    end
end
