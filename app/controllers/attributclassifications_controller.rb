class AttributclassificationsController < ApplicationController
  before_action :set_attributclassification, only: %i[ show edit update destroy ]

  # GET /attributclassifications or /attributclassifications.json
  def index
    @attributclassifications = Attributclassification.all
  end

  # GET /attributclassifications/1 or /attributclassifications/1.json
  def show
  end

  # GET /attributclassifications/new
  def new
    @attributclassification = Attributclassification.new
  end

  # GET /attributclassifications/1/edit
  def edit
  end

  # POST /attributclassifications or /attributclassifications.json
  def create
    @attributclassification = Attributclassification.new(attributclassification_params)

    respond_to do |format|
      if @attributclassification.save
        format.html { redirect_to attributclassification_url(@attributclassification), notice: "Attributclassification was successfully created." }
        format.json { render :show, status: :created, location: @attributclassification }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @attributclassification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attributclassifications/1 or /attributclassifications/1.json
  def update
    respond_to do |format|
      if @attributclassification.update(attributclassification_params)
        format.html { redirect_to attributclassification_url(@attributclassification), notice: "Attributclassification was successfully updated." }
        format.json { render :show, status: :ok, location: @attributclassification }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @attributclassification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attributclassifications/1 or /attributclassifications/1.json
  def destroy
    @attributclassification.destroy

    respond_to do |format|
      format.html { redirect_to attributclassifications_url, notice: "Attributclassification was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attributclassification
      @attributclassification = Attributclassification.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def attributclassification_params
      params.require(:attributclassification).permit(:code, :libelle, :type, :longueur, :nbdecimal, :zonefiltre, :classification_id)
    end
end
