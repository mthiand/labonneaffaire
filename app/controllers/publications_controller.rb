class PublicationsController < ApplicationController
  before_action :set_publication, only: %i[ show edit update destroy ]
  before_action :require_author, only: [:edit, :update, :destroy]

  # GET /publications or /publications.json
  def index
    @publications = Publication.all
  end

  # GET /publications or /publications.json
  def search
    wherevar = "titre not null";
    restrict = false;
    if params[:category_search].present?
      restrict = true;
      #wherevar = wherevar + " and category_id in ( SELECT Category.id where libelle LIKE '"+"%#{params[:category_search]}%"+"')";
    end
    if params[:libelle_search].present?
      restrict = true;
      wherevar = wherevar + " and titre LIKE '"+"%#{params[:libelle_search]}%"+"'";
    end
    if params[:ville_search].present?
      restrict = true;
      #wherevar = wherevar + " and (region.nom LIKE '"+"%#{params[:ville_search]}%"+"' or adresse LIKE '"+"%#{params[:ville_search]}%"+"')";
      wherevar = wherevar + " and adresse LIKE '"+"%#{params[:ville_search]}%"+"'";
    end
    @publications = Publication.where(wherevar)
            
  end

  # GET /publications/1 or /publications/1.json
  def show
  end

  # GET /publications/new
  def new
    @publication = Publication.new
  end

  # GET /publications/1/edit
  def edit
  end

  # POST /publications or /publications.json
  def create
    @publication = Publication.new(publication_params)
    @publication.user = current_user

    respond_to do |format|
      if @publication.save
        format.html { redirect_to publication_url(@publication), notice: "Publication was successfully created." }
        format.json { render :show, status: :created, location: @publication }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /publications/1 or /publications/1.json
  def update
    respond_to do |format|
      if @publication.update(publication_params)
        format.html { redirect_to publication_url(@publication), notice: "Publication was successfully updated." }
        format.json { render :show, status: :ok, location: @publication }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publications/1 or /publications/1.json
  def destroy
    @publication.destroy

    respond_to do |format|
      format.html { redirect_to publications_url, notice: "Publication was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_publication
      @publication = Publication.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def publication_params
      params.fetch(:publication, {}).permit(:titre, :description, :typeannonce, :prix, :livrable, :adresse, :latitude, :longitude, :paysannonce_id, :region_id, :departement_id, :arrondissement_id, :classification_id, :user_id, :icone)
    end
  
    def require_author
      redirect_to(search_publications_path) unless @publication.user == current_user
    end

end
