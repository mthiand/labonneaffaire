class PaysannoncesController < ApplicationController
  before_action :set_paysannonce, only: %i[ show edit update destroy ]

  # GET /paysannonces or /paysannonces.json
  def index
    @paysannonces = Paysannonce.all
  end

  # GET /paysannonces/1 or /paysannonces/1.json
  def show
  end

  # GET /paysannonces/new
  def new
    @paysannonce = Paysannonce.new
  end

  # GET /paysannonces/1/edit
  def edit
  end

  # POST /paysannonces or /paysannonces.json
  def create
    @paysannonce = Paysannonce.new(paysannonce_params)

    respond_to do |format|
      if @paysannonce.save
        format.html { redirect_to paysannonce_url(@paysannonce), notice: "Paysannonce was successfully created." }
        format.json { render :show, status: :created, location: @paysannonce }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @paysannonce.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paysannonces/1 or /paysannonces/1.json
  def update
    respond_to do |format|
      if @paysannonce.update(paysannonce_params)
        format.html { redirect_to paysannonce_url(@paysannonce), notice: "Paysannonce was successfully updated." }
        format.json { render :show, status: :ok, location: @paysannonce }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @paysannonce.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paysannonces/1 or /paysannonces/1.json
  def destroy
    @paysannonce.destroy

    respond_to do |format|
      format.html { redirect_to paysannonces_url, notice: "Paysannonce was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paysannonce
      @paysannonce = Paysannonce.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def paysannonce_params
      params.require(:paysannonce).permit(:code, :nom, :latitude, :longitude)
    end
end
