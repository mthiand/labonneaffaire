class PiecejointesController < ApplicationController
  before_action :set_piecejointe, only: %i[ show edit update destroy ]

  # GET /piecejointes or /piecejointes.json
  def index
    @piecejointes = Piecejointe.all
  end

  # GET /piecejointes/1 or /piecejointes/1.json
  def show
  end

  # GET /piecejointes/new
  def new
    @piecejointe = Piecejointe.new
  end

  # GET /piecejointes/1/edit
  def edit
  end

  # POST /piecejointes or /piecejointes.json
  def create
    @piecejointe = Piecejointe.new(piecejointe_params)

    respond_to do |format|
      if @piecejointe.save
        format.html { redirect_to piecejointe_url(@piecejointe), notice: "Piecejointe was successfully created." }
        format.json { render :show, status: :created, location: @piecejointe }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @piecejointe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /piecejointes/1 or /piecejointes/1.json
  def update
    respond_to do |format|
      if @piecejointe.update(piecejointe_params)
        format.html { redirect_to piecejointe_url(@piecejointe), notice: "Piecejointe was successfully updated." }
        format.json { render :show, status: :ok, location: @piecejointe }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @piecejointe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /piecejointes/1 or /piecejointes/1.json
  def destroy
    @piecejointe.destroy

    respond_to do |format|
      format.html { redirect_to piecejointes_url, notice: "Piecejointe was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_piecejointe
      @piecejointe = Piecejointe.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def piecejointe_params
      params.require(:piecejointe).permit(:nom, :pj_file_name, :pj_file_size, :pj_content_type, :pj_updated_at, :publication_id, :pj)
    end
end
