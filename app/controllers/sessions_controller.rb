class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      redirect_to search_publications_path
    else
      flash.now[:danger] = 'Login/mot de passe incorrect. Réessayer à nouveau.'
      render 'new'
    end
  end

  def destroy
    session.delete(:user_id)
    @current_user = nil
    redirect_to search_publications_path
  end
  
end
